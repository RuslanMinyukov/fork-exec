#include <thread>
#include <iostream>
#include <unistd.h>

void ThreadFunc()
{
	sleep(300);
}

int main()
{
	const int rc = daemon(0, 0);
	if (rc == 0) {
		std::thread thread(ThreadFunc);
		thread.join();
	}
	else {
		perror("daemon() failed");
	}
	return 0;
}

