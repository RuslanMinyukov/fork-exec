#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>

void ExecuteChildProcess()
{
	const int rc = execl("/home/ruslan/source/fork-exec/child", "child", (const char*)0);
	if (rc < 0)
	{
		perror("execl() failed");
	}
}

int main()
{
	std::cout << "This is the parent process" << std::endl;
	const pid_t pid = fork();
	switch (pid)
	{
		case -1:
			perror("fork() failed");						
			break;
		case 0:
			ExecuteChildProcess();
			break;
		default:
			std::cout << "fork() succeeded" << std::endl;
			break;
	}
	return 0;
}

